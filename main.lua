-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
local composer = require( "composer" )

local function permissionListener( event )
    for key,value in pairs( event.grantedAppPermissions ) do
        if ( value == "Microphone" ) then
            microphoneAccess = true
            print( "Microphone permission granted!" )
        end
    end

    if microphoneAccess == false then
        print( "Microphone permission not granted" )
    end
end

local function showPermissionsPopup()
    local options = {
        appPermission = "Microphone",
        urgency = "Critical",
        listener = permissionListener,
        rationaleTitle = "Microphone access required",
        rationaleDescription = "Microphone access is required to take photos. Re-request now?",
        settingsRedirectTitle = "Alert",
        settingsRedirectDescription = "Without the ability to use microphone, this app cannot properly function. Please grant microphone access within Settings."
    }
    
    native.showPopup( "requestAppPermission", options )
end

-- Hide status bar
display.setStatusBar( display.HiddenStatusBar )

showPermissionsPopup()

-- Go to the menu screen
composer.gotoScene( "menu" )