math.randomseed( os.time() )

local random = {}

function random.shuffle( t )
    if ( type(t) ~= "table" ) then
        print( "WARNING: shuffle() function expects a table" )
        return false
    end
 
    local j
 
    for i = #t, 2, -1 do
        j = math.random( i )
        t[i], t[j] = t[j], t[i]
    end
    
    return t
end

return random