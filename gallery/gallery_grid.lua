
local composer = require( "composer" )
local widget = require( "widget" )
local json = require( "json" )
local audio = require( "audio" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function goBack()
	composer.gotoScene( "gallery.gallery_list" )
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

    local newBackBtn = widget.newButton(
		{
			top = 800,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/back.png",
			overFile = "assets/ui/back.png",
			onEvent = handleButtonEvent
		}
	)	
	newBackBtn.x = display.contentCenterX
	newBackBtn:addEventListener( "tap", goBack )
	sceneGroup:insert( newBackBtn )

	local itemsFile = system.pathForFile( "data/items.json", system.ResourceDirectory )
	local allItems = json.decodeFile( itemsFile )
	local items = allItems[event.params.category]

	local total = 1
	local rows = math.ceil( #items / 4)

	for i = 1, rows do
		local x = 80 + i * 120
		local y = 100

		for j = 1,4 do
			y = 50 + j * 120

			print(total)

			if items[total] == nil then 
				break
			end

			local sound = audio.loadSound( items[total].sound )

			local itemObject = display.newImageRect( items[total].image, 100, 100 )
			itemObject.x = x
			itemObject.y = y
			itemObject:addEventListener( "tap", function() audio.play ( sound ) end )
			sceneGroup:insert( itemObject )

			total = total + 1
		end

		
	end

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
