local composer = require( "composer" )
local widget = require( "widget" )
local json = require( "json" )
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function gotoList()
	composer.gotoScene( "collection.collection_list" )
end
-- Function to pass parameters on click
local function chooseWord(items)
    composer.gotoScene( "collection.collection_edit", {
     params = { items = items }
    })
   end

-- ScrollView listener
local function scrollListener( event )
 
    local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end
 
    -- In the event a scroll limit is reached...
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
 
    return true
end

local function handleButtonEvent( event )
    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
    end

end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
    -- Create the scrollView widget------------------------------------------------
   
    local itemsFile = system.pathForFile( "data/items.json", system.ResourceDirectory )
	local items = json.decodeFile( itemsFile )

    local scrollView = widget.newScrollView(
    {
        top = 100,
        width = 500,
        height = 600,
        --scrollWidth = 400,
        --scrollHeight = 800,
        hideBackground = true,
        listener = scrollListener,
        horizontalScrollDisabled = true
    }
    )
    scrollView.x = display.contentCenterX
    sceneGroup:insert( scrollView )

    for i = 1, #items.fruits do

    -- Cration of the group
    local buttonGroup = display.newGroup()
	local singleRow = widget.newButton(
		{
			--top = 700,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/top_bar.png",
			overFile = "assets/ui/top_bar_green.png",
			onEvent = handleButtonEvent
		}
	)	
    --local singleRow = display.newImage( "assets/ui/top_bar.png")
    local singleItemText = display.newText( items.fruits[i].name, 0, 0, "CarterOne.ttf", 35 )
    local singleItemTextShadow = display.newText( items.fruits[i].name, 0, 0, "CarterOne.ttf", 35 )
    singleRow.x = 250
    singleRow.y = i * 100
    singleItemText.x = 250
    singleItemText.y = i * 100
    singleItemTextShadow.x = 252
    singleItemTextShadow.y = i * 100 + 2 
    singleItemTextShadow:setFillColor( 0 )
    scrollView:insert(singleRow)
    scrollView:insert(buttonGroup)

    -- using the group to layer the text
    buttonGroup:insert(1, singleItemTextShadow)
    buttonGroup:insert(2, singleItemText)

    singleRow:addEventListener( "tap", function() chooseWord(items.fruits[i].key) end)
    end

	--------------------------------------------------------------------

	local newBackBtn = widget.newButton(
		{
			top = 800,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/back.png",
			overFile = "assets/ui/back.png",
			onEvent = handleButtonEvent
		}
	)	
	newBackBtn.x = display.contentCenterX
	sceneGroup:insert( newBackBtn )
    
    newBackBtn:addEventListener( "tap", gotoList )


end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
