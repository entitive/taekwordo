
local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function gotoList()
	composer.gotoScene( "collection.collection_list" )
end
local function gotoWords()
	composer.gotoScene( "collection.collection_words" )
end

-- ScrollView listener
local function scrollListener( event )
 
    local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end
 
    -- In the event a scroll limit is reached...
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
 
    return true
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
	
	-- Create the scrollView widget------------------------------------------------
    local scrollView = widget.newScrollView(
    {
        top = 100,
        --left = 100,
        width = 500,
        height = 600,
        --scrollWidth = 400,
        --scrollHeight = 800,
        hideBackground = true,
        listener = scrollListener,
        horizontalScrollDisabled = true
    }
    )
    scrollView.x = display.contentCenterX
    sceneGroup:insert( scrollView )
	--------------------------------------------------------------------

	local customCategory0 = display.newImage( "assets/ui/top_bar.png", 250, 80 )
	scrollView:insert( customCategory0 )
	local customCategoryExpand0 = display.newImage( "assets/ui/button_expand.png", 440, 80 )
	scrollView:insert( customCategoryExpand0 )
	customCategoryExpand0:addEventListener( "tap", gotoWords )

	local customCategory2 = display.newImage( "assets/ui/top_bar.png", 250, 180 )
	scrollView:insert( customCategory2 )
	local customCategoryExpand2 = display.newImage( "assets/ui/button_expand.png", 440, 180 )
	scrollView:insert( customCategoryExpand2 )
	customCategoryExpand2:addEventListener( "tap", gotoWords )

	local customCategory3 = display.newImage( "assets/ui/top_bar.png", 250, 280 )
	scrollView:insert( customCategory3 )
	local customCategoryExpand3 = display.newImage( "assets/ui/button_expand.png", 440, 280 )
	scrollView:insert( customCategoryExpand3 )
	customCategoryExpand3:addEventListener( "tap", gotoWords )

	local customCategory4 = display.newImage( "assets/ui/top_bar.png", 250, 380 )
	scrollView:insert( customCategory4 )
	local customCategoryExpand4 = display.newImage( "assets/ui/button_expand.png", 440, 380 )
	scrollView:insert( customCategoryExpand4 )
	customCategoryExpand4:addEventListener( "tap", gotoWords )

	local customCategory5 = display.newImage( "assets/ui/top_bar.png", 250, 480 )
	scrollView:insert( customCategory5 )
	local customCategoryExpand5 = display.newImage( "assets/ui/button_expand.png", 440, 480 )
	scrollView:insert( customCategoryExpand5 )
	customCategoryExpand5:addEventListener( "tap", gotoWords )

	local newBackBtn = widget.newButton(
		{
			top = 800,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/back.png",
			overFile = "assets/ui/back.png",
			onEvent = handleButtonEvent
		}
	)	
	newBackBtn.x = display.contentCenterX
	sceneGroup:insert( newBackBtn )
    
    newBackBtn:addEventListener( "tap", gotoList )

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
