
local composer = require( "composer" )
local widget = require( "widget" )
local json = require( "json" )
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function chooseCategory(category)
    composer.gotoScene( "collection.collection_edit", {
     params = { category = category }
    })
   end

local function gotoEdit()
	composer.gotoScene( "collection.collection_edit" )
end

local function goBack()
	composer.gotoScene( "menu" )
end

-- ScrollView listener
local function scrollListener( event )
 
    local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end
 
    -- In the event a scroll limit is reached...
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
 
    return true
end

local function handleButtonEvent( event )
    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
    end

end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

	local topBar = display.newImage( "assets/ui/top_bar.png", display.contentCenterX, 40 )
	topBar.width = 800
	sceneGroup:insert( topBar )
	
	local buttonPlus = display.newImage( "assets/ui/button_plus.png", 140, 40 )
	sceneGroup:insert( buttonPlus )

	local topEdit = display.newText( "Edit", 600, 40, "CarterOne.ttf", 40 )
	sceneGroup:insert( topEdit )

	-- Create the scrollView widget------------------------------------------------
   
	local categoriesFile = system.pathForFile( "data/categories.json", system.ResourceDirectory )
	local categories = json.decodeFile( categoriesFile )

    local scrollView = widget.newScrollView(
    {
        top = 100,
        width = 500,
        height = 600,
        --scrollWidth = 400,
        --scrollHeight = 800,
        hideBackground = true,
        listener = scrollListener,
        horizontalScrollDisabled = true
    }
    )
    scrollView.x = display.contentCenterX
    sceneGroup:insert( scrollView )

    for i = 1, #categories.list do

    -- Cration of the group
    local buttonGroup = display.newGroup()

	local customCategoryExpand = display.newImage( "assets/ui/button_expand.png" )
	scrollView:insert( customCategoryExpand )
	
	local singleRow = widget.newButton(
		{
			--top = 700,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/top_bar.png",
			overFile = "assets/ui/top_bar_green.png",
			onEvent = handleButtonEvent
		}
	)
	
    --local singleRow = display.newImage( "assets/ui/top_bar.png")
	local singleRowText = display.newText( categories.list[i].name, 0, 0, "CarterOne.ttf", 35 )
	
    local singleRowTextShadow = display.newText( categories.list[i].name, 0, 0, "CarterOne.ttf", 35 )
    singleRow.x = 250
	singleRow.y = i * 100
	singleRow:toBack()
	customCategoryExpand.x = 440
	customCategoryExpand.y = i * 100
	customCategoryExpand:toFront()
    singleRowText.x = 250
    singleRowText.y = i * 100
    singleRowTextShadow.x = 252
    singleRowTextShadow.y = i * 100 + 2 
    singleRowTextShadow:setFillColor( 0 )
    scrollView:insert(singleRow)
    scrollView:insert(buttonGroup)

	-- using the group to layer the text
    buttonGroup:insert(1, singleRow)
	buttonGroup:insert(2, singleRowTextShadow)
	buttonGroup:insert(3, singleRowText)
	buttonGroup:insert(4, customCategoryExpand)


    singleRow:addEventListener( "tap", function() chooseCategory(categories.list[i].key) end)
    end
	
	
	
	--[[local scrollView = widget.newScrollView(
    {
        top = 100,
        --left = 100,
        width = 500,
        height = 600,
        --scrollWidth = 400,
        --scrollHeight = 800,
        hideBackground = true,
        listener = scrollListener,
        horizontalScrollDisabled = true
    }
    )
    scrollView.x = display.contentCenterX
    sceneGroup:insert( scrollView )
	--------------------------------------------------------------------
	
	local customCategory0 = display.newImage( "assets/ui/top_bar.png", 250, 80 )
	scrollView:insert( customCategory0 )
	local customCategoryExpand0 = display.newImage( "assets/ui/button_expand.png", 440, 80 )
	scrollView:insert( customCategoryExpand0 )
	customCategoryExpand0:addEventListener( "tap", gotoEdit )

	local customCategory2 = display.newImage( "assets/ui/top_bar.png", 250, 180 )
	scrollView:insert( customCategory2 )
	local customCategoryExpand2 = display.newImage( "assets/ui/button_expand.png", 440, 180 )
	scrollView:insert( customCategoryExpand2 )
	customCategoryExpand2:addEventListener( "tap", gotoEdit )

	local customCategory3 = display.newImage( "assets/ui/top_bar.png", 250, 280 )
	scrollView:insert( customCategory3 )
	local customCategoryExpand3 = display.newImage( "assets/ui/button_expand.png", 440, 280 )
	scrollView:insert( customCategoryExpand3 )
	customCategoryExpand3:addEventListener( "tap", gotoEdit )

	local customCategory4 = display.newImage( "assets/ui/top_bar.png", 250, 380 )
	scrollView:insert( customCategory4 )
	local customCategoryExpand4 = display.newImage( "assets/ui/button_expand.png", 440, 380 )
	scrollView:insert( customCategoryExpand4 )
	customCategoryExpand4:addEventListener( "tap", gotoEdit )

	local customCategory5 = display.newImage( "assets/ui/top_bar.png", 250, 480 )
	scrollView:insert( customCategory5 )
	local customCategoryExpand5 = display.newImage( "assets/ui/button_expand.png", 440, 480 )
	scrollView:insert( customCategoryExpand5 )
	customCategoryExpand5:addEventListener( "tap", gotoEdit )]]--



    local newContinueBtn = widget.newButton(
		{
			top = 800,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/continue.png",
			overFile = "assets/ui/continue.png",
			onEvent = handleButtonEvent
		}
	)	
	newContinueBtn.x = display.contentCenterX
	sceneGroup:insert( newContinueBtn )
    
	newContinueBtn:addEventListener( "tap", gotoEdit )

	local newBackBtn = widget.newButton(
		{
			top = 900,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/back.png",
			overFile = "assets/ui/back.png",
			onEvent = handleButtonEvent
		}
	)	
	newBackBtn.x = display.contentCenterX
	sceneGroup:insert( newBackBtn )

	newBackBtn:addEventListener( "tap", goBack )

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
