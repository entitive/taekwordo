local composer = require( "composer" )
local widget = require( "widget" )
local json = require( "json" )

local scene = composer.newScene()

local learningMode

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function chooseCategory(category)
    composer.gotoScene( "game.game_setup", {
        params = {
            category = category,
            learningMode = learningMode
        }
    })
end

local function scrollListener( event )
 
    local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end
 
    -- In the event a scroll limit is reached...
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
 
    return true
end


local function handleButtonEvent( event )
    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
    end

end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )
    local sceneGroup = self.view
    
    local categoriesFile = system.pathForFile( "data/categories.json", system.ResourceDirectory )
	local categories = json.decodeFile( categoriesFile )

    local scrollView = widget.newScrollView({
        top = 100,
        width = 500,
        height = 600,
        --scrollWidth = 400,
        --scrollHeight = 800,
        hideBackground = true,
        listener = scrollListener,
        horizontalScrollDisabled = true
    })
    scrollView.x = display.contentCenterX
    sceneGroup:insert( scrollView )

    for i = 1, #categories.list do
        -- Cration of the group
        local buttonGroup = display.newGroup()
        
        local singleRow = widget.newButton(
            {
                --top = 700,
                --width = 250,
                --height = 64,
                defaultFile = "assets/ui/top_bar.png",
                overFile = "assets/ui/top_bar_green.png",
                onEvent = handleButtonEvent
            }
        )	
        --local singleRow = display.newImage( "assets/ui/top_bar.png")
        local singleRowText = display.newText( categories.list[i].name, 0, 0, "CarterOne.ttf", 35 )
        local singleRowTextShadow = display.newText( categories.list[i].name, 0, 0, "CarterOne.ttf", 35 )
        singleRow.x = 250
        singleRow.y = i * 100
        singleRowText.x = 250
        singleRowText.y = i * 100
        singleRowTextShadow.x = 252
        singleRowTextShadow.y = i * 100 + 2 
        singleRowTextShadow:setFillColor( 0 )
        scrollView:insert(singleRow)
        scrollView:insert(buttonGroup)

        -- using the group to layer the text
        buttonGroup:insert(1, singleRowTextShadow)
        buttonGroup:insert(2, singleRowText)

        singleRow:addEventListener( "tap", function() chooseCategory(categories.list[i].key) end)
    end
   

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
        if event.params.learningMode then
            learningMode = event.params.learningMode
        end
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
