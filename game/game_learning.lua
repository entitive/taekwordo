
local composer = require( "composer" )
local widget = require( "widget" )
local native = require( "native" )
local json = require( "json" )
local physics = require( "physics" )
local timer = require( "timer" )
local audio = require( "audio" )
local random = require( "lib.random" )

-- Voice-to-text plugin
local voiceToText = require "plugin.voiceToText"

local scene = composer.newScene()

-- Variables needed all over the scene
local microphoneAccess = false
local activeIndex = 0
local activeWord = ""
local activeObj
local points = 0
local life = 3
local learningMode = true
local heartsGroup

-- Array for heart objects
local lifeHearts = {}

-- Predefined variables for main objects
local resultTextField, turnTextField, newPointsField, spikes, invisibleBottomWall, invisibleMiddleWall

-- Predefined function variables
local spawnItem, handleVoiceRecognition

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function finishGame()
	composer.gotoScene( "game.game_summary", {
		params = {
			points = points,
			stars = #lifeHearts,
			win = #lifeHearts > 0
		}
	})
end

local function onItemCollision( self, event )
	if event.phase == "ended" then
		if event.other == spikes then
			voiceToText.stopRecording()

			self:applyLinearImpulse( 0.8, 2, self.x, self.y )
			physics.setGravity( 0, 12 )
			
			timer.performWithDelay( 10, function () physics.removeBody( spikes ) end)
			
			lifeHearts[#lifeHearts]:removeSelf() 
			table.remove( lifeHearts, #lifeHearts )
		elseif event.other == invisibleBottomWall then
			if activeObj then
				activeObj:removeSelf()
			end

			if #lifeHearts == 0 then
				finishGame()
			else
				physics.setGravity( 0, 2 )
				timer.performWithDelay( 500, spawnItem )
			end
		elseif event.other == invisibleMiddleWall and learningMode then
			physics.pause()

			audio.play(event.target.sound, {
				onComplete = function() 
					physics.setGravity( 0, 12 ) 
					physics.start()
				end
			})
			
			timer.performWithDelay( 10, function()
				physics.removeBody( invisibleMiddleWall	) 
				physics.removeBody( spikes ) 
			end)
		end
	end
end

local function createLifeHeart( index )
	local heart = display.newImage( "assets/ui/heart.png", 64, 64 )
	heart.x = display.contentCenterX - 140 + index * 70 
	heart.y = display.contentHeight - 300

	return heart
end

local function toggleFade( object, callback )
	transition.fadeIn( object, {
		time = 1000,
		onComplete = function() 
			timer.performWithDelay( 1000, function()
				transition.fadeOut( object, {
					time = 1000,
					onComplete = function()
						if callback then
							callback()
						end
					end
				})
			end)
		end
	})
end

spawnItem = function()
	if activeIndex > 0 and learningMode then
		learningMode = false
		toggleFade(turnTextField, function()
			heartsGroup.alpha = 1
			resultTextField.alpha = 1
		end)
	else
		learningMode = true
		activeIndex = activeIndex + 1
		physics.addBody( invisibleMiddleWall, "static" )
		heartsGroup.alpha = 0
		resultTextField.alpha = 0
	end

	print("activeIndex: " .. activeIndex)
	print("#items: " .. #items)

	if activeIndex > #items then
		return finishGame()
	end

	physics.addBody( spikes, "static" )

	local item = items[activeIndex]
	local itemObject = display.newImageRect( item.image, 200, 200 )
	itemObject.sound = audio.loadSound( item.sound )
	
	itemObject.x = display.contentCenterX
	itemObject.y = -600
	
	itemObject.collision = onItemCollision
	itemObject:addEventListener( "collision" )  

	physics.addBody( itemObject, "dynamic", { bounce = 0.5 } )

	activeObj = itemObject
	activeWord = item.name

	timer.performWithDelay( 1000, voiceToText.startRecording )
end

function success()
	voiceToText.stopRecording()
	activeObj:removeSelf() 

	points = points + 10
	toggleFade(newPointsField)
	resultTextField.text = "Points: " .. points
	
	spawnItem()
end

handleVoiceRecognition = function( event )
	if event then
		print( json.encode( event ) )
	end

	local activeItem = items[activeIndex] 
	local result = ""

	if event.speech then
		result = string.lower(event.speech)
	end

	if string.find( result, activeItem.name ) then
		success()
	end

	if event.response == "stopped" then
		resultTextField.text = ""
	end
end


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )
	
end

-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
		system.setIdleTimer( true ) 
		
		points = 0
		stars = 0
		life = 3
		activeIndex = 0

		heartsGroup = display.newGroup()
		
		physics.start()
		physics.setGravity( 0, 2 )

		local sceneGroup = self.view

		spikes = display.newImageRect( "assets/ui/spikes.png", 3000, 150 )
		spikes.x = display.contentCenterX
		spikes.y = display.contentHeight - 70
		sceneGroup:insert( spikes )

		invisibleBottomWall = display.newRect( display.contentCenterX, display.contentHeight + 300, 3000, 1 )
		sceneGroup:insert( invisibleBottomWall )
		physics.addBody( invisibleBottomWall, "static" )

		invisibleMiddleWall = display.newRect( display.contentCenterX, display.contentCenterY, 3000, 1 )
		sceneGroup:insert( invisibleMiddleWall )
		physics.addBody( invisibleMiddleWall, "static" )

		local itemsFile = system.pathForFile( "data/items.json", system.ResourceDirectory )
		local allItems = json.decodeFile( itemsFile )
		items =	random.shuffle( allItems[event.params.category] )
		
		resultTextField = display.newText( "Points: " .. points, display.contentCenterX, display.screenOriginY + 100 , "CarterOne.ttf", 40 )
		resultTextField:setFillColor( 1, 0, 0 )
		resultTextField.alpha = 0
		sceneGroup:insert( resultTextField )

		turnTextField = display.newText( "Your turn", display.contentCenterX, display.screenOriginY + 300 , "CarterOne.ttf", 40 )
		turnTextField:setFillColor( 1, 0, 0 )
		turnTextField.alpha = 0
		sceneGroup:insert( turnTextField )

		newPointsField = display.newText( "+10 Points!", display.contentCenterX, display.screenOriginY + 400 , "CarterOne.ttf", 50 )
		newPointsField:setFillColor( 1, 0, 0 )
		newPointsField.alpha = 0
		sceneGroup:insert( newPointsField )

		for i=1, 3 do
			lifeHearts[i] = createLifeHeart( i )
			heartsGroup:insert( lifeHearts[i] )
		end

		heartsGroup.alpha = 0
		sceneGroup:insert( heartsGroup )

		voiceToText.init(handleVoiceRecognition)

		spawnItem()
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		for i = sceneGroup.numChildren, 1, -1 do
			sceneGroup[i]:removeSelf()
			sceneGroup[1] = nil
		end
		-- Code here runs immediately after the scene goes entirely off screen
		physics.stop()
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
