
local composer = require( "composer" )
local widget = require( "widget" )
local timer = require( "timer" )

local scene = composer.newScene()

local counter = 3
local counterTextField, startBtn, categoryContainer, chooseCategoryBtn
local category = "fruits"
local learningMode = false

local textOnButton

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function startGame()
	local sceneName = learningMode and "game.game_learning" or "game.game_main"
	
	composer.gotoScene(sceneName, {
		effect = "fade",
		params = {
			category = category
		}
	})
end

local function chooseCategory()
	composer.gotoScene( "game.game_category_list", {
		params = {
			learningMode = learningMode
		}
	})
end

local function goBack()
	composer.gotoScene( "menu" )
end

local function handleButtonEvent( event )
    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
    end
end

local function countDown()
	if counter == 0 then
		startGame()
	else
		counterTextField.text = "Get ready! " .. counter
		counter = counter - 1
		timer.performWithDelay( 1000, countDown )
	end
end



-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

end


-- show()
function scene:show( event )

	counter = 3

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

		
	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
		if event.params and event.params.category then
			category = event.params.category and event.params.category or "fruits"
		end

		if event.params and event.params.learningMode then
			learningMode = event.params.learningMode
		end
		
		startBtn = widget.newButton(
			{
				top = 800,
				--width = 250,
				--height = 64,
				defaultFile = "assets/ui/start.png",
				overFile = "assets/ui/start.png",
				onEvent = handleButtonEvent
			}
		)	
		startBtn.x = display.contentCenterX
		startBtn:addEventListener( "tap", function()
			startBtn:removeSelf()
			backBtn:removeSelf()
			categoryContainer:removeSelf()
			chooseCategoryBtn:removeSelf()
			textOnButton:removeSelf()
			countDown()
		end)
		sceneGroup:insert( startBtn )

		backBtn = widget.newButton(
			{
				top = 900,
				--width = 250,
				--height = 64,
				defaultFile = "assets/ui/back.png",
				overFile = "assets/ui/back.png",
				onEvent = handleButtonEvent
			}
		)	
		backBtn.x = display.contentCenterX
		backBtn:addEventListener( "tap", goBack )
		sceneGroup:insert( backBtn )

		counterTextField = display.newText( "", display.contentCenterX, display.contentCenterY , "CarterOne.ttf", 40 )
		counterTextField:setFillColor( 0, 0.5, 1 )
		sceneGroup:insert( counterTextField )

		categoryContainer = display.newImage( "assets/ui/game_setting_border.png", display.contentCenterX, 200 )
		sceneGroup:insert( categoryContainer )

		-- Button to choose category
		chooseCategoryBtn = widget.newButton(
			{
				top = 220,
				--width = 250,
				--height = 64,
				defaultFile = "assets/ui/game_settings_cat_list_btn.png",
				overFile = "assets/ui/game_settings_cat_list_btn.png",
				onEvent = handleButtonEvent
			}
		)	
		chooseCategoryBtn.x = display.contentCenterX
		sceneGroup:insert( chooseCategoryBtn )
		chooseCategoryBtn:addEventListener( "tap", chooseCategory)

		textOnButton = display.newText( "Tap to choose", display.contentCenterX, 240, "CarterOne.ttf", 25 )
		sceneGroup:insert( textOnButton )

		if event.params and event.params.category then
			textOnButton.text = event.params.category
		end
	end

	
end


-- hide()
function scene:hide( event )
	scene:destroy()

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		for i = sceneGroup.numChildren, 1, -1 do
			sceneGroup[i]:removeSelf()
			sceneGroup[1] = nil
		end
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
