
local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function gotoMenu()
	composer.gotoScene( "menu" )
end

local function handleButtonEvent( event )
 
    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
    end
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )
end


-- show()
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
		-- Code here runs when the scene is first created but has not yet appeared on screen

		local scoreTextField = display.newText( "Score: " .. event.params.points, display.contentCenterX, display.contentCenterY , native.systemFont )
		sceneGroup:insert( scoreTextField )
		local winIcon = display.newImage( "assets/ui/stars_"..event.params.stars..".png", display.contentCenterX, 350 )
		sceneGroup:insert( winIcon )

		local displayResult = display.newText( event.params.win and "YOU WIN!" or "GAME OVER", display.contentCenterX, 350, "CarterOne.ttf", 48 )
		sceneGroup:insert( displayResult )
		if event.params.win then
			displayResult:setFillColor( 0, 0, 1 )
		else
			displayResult:setFillColor( 1, 0, 0 )
		end
		
		local displayScore = display.newText( "Score: " .. event.params.points, display.contentCenterX, 440, "CarterOne.ttf", 30 )
		displayScore:setFillColor( 0, 0, 1 )
		sceneGroup:insert( displayScore )

		local displayMistakes = display.newText( "Mistakes: " .. (3 - event.params.stars), display.contentCenterX, 480, "CarterOne.ttf", 30 )
		displayMistakes:setFillColor( 0, 0, 1 )
		sceneGroup:insert( displayMistakes )

		local newContinueBtn = widget.newButton(
			{
				top = 800,
				--width = 250,
				--height = 64,
				defaultFile = "assets/ui/continue.png",
				overFile = "assets/ui/continue.png",
				onEvent = handleButtonEvent
			}
		)	
		newContinueBtn.x = display.contentCenterX
		sceneGroup:insert( newContinueBtn )
		newContinueBtn:addEventListener( "tap", gotoMenu )
	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

	sceneGroup:removeSelf()

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
