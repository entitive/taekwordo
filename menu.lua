
local composer = require( "composer" )
local native = require( "native" )
local widget = require( "widget" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function gotoGame()
	composer.gotoScene( "game.game_setup" )
end

local function gotoLearning()
	composer.gotoScene( "game.game_setup", {
		params = { learningMode = true }
	})
end

local function gotoGallery()
	composer.gotoScene( "gallery.gallery_list" )
end

local function gotoEditCollection()
	composer.gotoScene( "collection.collection_list" )
end

local function gotoShop()
	composer.gotoScene( "shop" )
end

local function gotoSettings()
	composer.gotoScene( "settings" )
end

local function exitGame()
	native.requestExit()
end

local function handleButtonEvent( event )
    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
    end
end



-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

	local soundFile = audio.loadStream("assets/music/music.mp3")
 
	-- audio.play(soundFile)

	---------------------------------------LOGO---------------------------------------------
	local logo = display.newImage( "assets/ui/logo-minimal.png", display.contentCenterX, 200 )
	sceneGroup:insert( logo )
	----------------------------------------MENU BUTTONS------------------------------------
	
	local newGameBtn = widget.newButton({
		top = 400,
		--width = 250,
		--height = 64,
		defaultFile = "assets/ui/button_menu.png",
		overFile = "assets/ui/button_menu_pressed.png",
		onEvent = handleButtonEvent,
	})	
	newGameBtn.x = display.contentCenterX
	newGameBtn:addEventListener( "tap", gotoGame )
	sceneGroup:insert( newGameBtn )
	
	local newGameBtnTxt = display.newText( "New Game", display.contentCenterX, 430, "CarterOne.ttf", 30 )
	sceneGroup:insert( newGameBtnTxt ) 

	local learningBtn = widget.newButton(
		{
			top = 500,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/button_menu.png",
			overFile = "assets/ui/button_menu_pressed.png",
			onEvent = handleButtonEvent,
		}
	)	
	learningBtn.x = display.contentCenterX
	learningBtn:addEventListener( "tap", gotoLearning )
	sceneGroup:insert( learningBtn )
	
	local learningBtnTxt = display.newText( "Learning", display.contentCenterX, 530, "CarterOne.ttf", 30 )
	sceneGroup:insert( learningBtnTxt ) 
	
	local galleryBtn = widget.newButton(
		{
			top = 600,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/button_menu.png",
			overFile = "assets/ui/button_menu_pressed.png",
			onEvent = handleButtonEvent
		}
	)	
	galleryBtn.x = display.contentCenterX
	galleryBtn:addEventListener( "tap", gotoGallery )
	sceneGroup:insert( galleryBtn )

	local galleryBtnTxt = display.newText( "Gallery", display.contentCenterX, 630, "CarterOne.ttf", 30 )
	sceneGroup:insert( galleryBtnTxt ) 

	--[[local editCollectionBtn = widget.newButton(
		{
			top = 500,
			--width = 250,
			--height = 64,
			defaultFile = "assets/ui/edit_collection.png",
			overFile = "assets/ui/edit_collection_pressed.png",
			onEvent = handleButtonEvent
		}
	)	
	editCollectionBtn.x = display.contentCenterX
	sceneGroup:insert( editCollectionBtn )--]]--

	-- local shopBtn = widget.newButton(
	-- 	{
	-- 		top = 600,
	-- 		--width = 250,
	-- 		--height = 64,
	-- 		defaultFile = "assets/ui/button_menu.png",
	-- 		overFile = "assets/ui/button_menu_pressed.png",
	-- 		onEvent = handleButtonEvent
	-- 	}
	-- )	
	-- shopBtn.x = display.contentCenterX
	-- sceneGroup:insert( shopBtn )

	-- local shopBtnTxt = display.newText( "Shop", display.contentCenterX, 630, "CarterOne.ttf", 30 )
	-- sceneGroup:insert( shopBtnTxt ) 

	-- local settingsBtn = widget.newButton(
	-- 	{
	-- 		top = 700,
	-- 		--width = 250,
	-- 		--height = 64,
	-- 		defaultFile = "assets/ui/button_menu.png",
	-- 		overFile = "assets/ui/button_menu_pressed.png",
	-- 		onEvent = handleButtonEvent
	-- 	}
	-- )	
	-- settingsBtn.x = display.contentCenterX
	-- sceneGroup:insert( settingsBtn )

	-- local settingsBtnTxt = display.newText( "Settings", display.contentCenterX, 730, "CarterOne.ttf", 30 )
	-- sceneGroup:insert( settingsBtnTxt ) 
	local platform = system.getInfo( "platform" )

	if platform ~= "ios" then
		local exitBtn = widget.newButton(
			{
				top = 700,
				--width = 250,
				--height = 64,
				defaultFile = "assets/ui/button_menu.png",
				overFile = "assets/ui/button_menu_pressed.png",
				onEvent = handleButtonEvent,
			}
		)	
		exitBtn.x = display.contentCenterX
		exitBtn:addEventListener( "tap", exitGame )
		sceneGroup:insert( exitBtn )

		local exitBtnTxt = display.newText( "Exit", display.contentCenterX, 730, "CarterOne.ttf", 30 )
		sceneGroup:insert( exitBtnTxt ) 
	end

	--local newGameBtn = display.newText( sceneGroup, "New Game", display.contentCenterX, 200, native.systemFont, 44 )
	--newGameBtn:setFillColor( black )

	--local galleryBtn = display.newText( sceneGroup, "Gallery", display.contentCenterX, 300, native.systemFont, 44 )
	--galleryBtn:setFillColor( black )

	--local editCollectionBtn = display.newText( sceneGroup, "Edit Collection", display.contentCenterX, 400, native.systemFont, 44 )
	--editCollectionBtn:setFillColor( black )

	--local shopBtn = display.newText( sceneGroup, "Shop", display.contentCenterX, 500, native.systemFont, 44 )
	--shopBtn:setFillColor( black )

	--local settingsBtn = display.newText( sceneGroup, "Settings", display.contentCenterX, 600, native.systemFont, 44 )
	--settingsBtn:setFillColor( black )

	--local exitBtn = display.newText( sceneGroup, "Exit", display.contentCenterX, 810, native.systemFont, 44 )
	--exitBtn:setFillColor( black )
	
	--editCollectionBtn:addEventListener( "tap", gotoEditCollection )
	-- shopBtn:addEventListener( "tap", gotoShop )
	-- settingsBtn:addEventListener( "tap", gotoSettings )
	
	local background = display.newImage( "assets/ui/sky.jpg", 640, 512 )
	background:toBack()
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	
	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
